﻿/*Title: Enemy properties
 * Author: Rocky
 * Purpose: Provides characteristics to the enemy on the game
 * Last Edited: 31/01/2020
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{


	public float speed = 0.50F;		//the speed of enemy
	public bool movingRight = true;
	public float stepsCounter;      //the number of steps to flip the enemy's direction.
	public float maxSteps = 100;    //the length of distance for the enemy to walk.

	private Animator anim;
	
	void Start()
	{
		anim = GetComponent<Animator>();
	}

	//-------This update function is called every frame of the game-------
	void Update()
	{
		//-----This will move the enemy------
		transform.Translate(Vector2.right * speed * Time.deltaTime * 2);
		stepsCounter++;

		//to flip the enemy after certain footsteps.
		if (stepsCounter == maxSteps)
		{
			Flip();
			stepsCounter = 0;
		}
	}

	//----------This will flip the enemy horizontally-------------------------------
	private void Flip()
	{
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		speed *= -1;
	}

	//-----------This will attack the player when in near----------------------------
	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag.Equals("Player"))
		{
			print("Collision detected");    //for debugging in the unity console
			anim.SetTrigger("isCollided");
			
			//rest code still remaining
			//
			//
		}
	}

}
