﻿/* Title: PlayerMovement script
 * Author: Jessica McKendry
 * Purpose: Provides functions for moving & controlling the Player character
 * Last modified: 30/1/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float walkSpeed = 5F;     //player walk speed
    public float climbSpeed = 10F;      //player ladder climbing speed
    public float jumpForce = 12F;      //force of the jump
    public bool hasLanded;      //if the player has landed
    private bool facingRight = true;    //direction that player sprite is facing

    private Rigidbody2D playerBody;
    private PlayerAnimation playerAnimation;    //calls in PlayerAnimation script

    // private BoxCollider2D boxCollider;    //for collision detection and jumping
    //private SpriteRenderer spriteRenderer;
    // private float gravityScaleAtStart;

    void Awake() 
    {
        playerBody = GetComponent<Rigidbody2D>();
        playerAnimation = GetComponent<PlayerAnimation>();

        //spriteRenderer = GetComponent<SpriteRenderer>();
        //boxCollider = GetComponent<BoxCollider2D>();

        //gravityScaleAtStart = playerBody.gravityScale;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Walk(); //default mode, will test for idle

        if (Input.GetButtonDown("Vertical"))    //player presses up/down
        {
            //will test for ladders in Part C
            Climb();
        }
        if (hasLanded && Input.GetButtonDown("Jump"))    //player presses spacebar
        {
            Jump();
        }
        if (Input.GetButtonDown("Attack1")) //player presses attack button
        {
            //will add & test for enemy collision in Part C
            Attack();
        }
        //test for player has landed
        if (hasLanded)
        {
            OnLanding();
        }
    }

    // Walk is called when the user presses right or left
    void Walk()
    {
        float h = Input.GetAxisRaw("Horizontal");   //determines right or left movement
        //if the player is facing right but moving left
        if (facingRight && h < 0)
        {
            Flip();
        }
        else if (!facingRight && h > 0) //if the player is not facing right while moving right
        {
            Flip();
        }
        
        //start walk animation
        playerAnimation.Walk(h);
        //move the player sprite
        playerBody.velocity = new Vector2(h * walkSpeed, playerBody.velocity.y);
    }

    //Jump is called when the player presses the spacebar
    void Jump()
    {
        hasLanded = false;
        playerBody.velocity = Vector2.up * jumpForce;
        playerAnimation.Jump(true);
    }

    //OnLanding is called when the player lands
    public void OnLanding()
    {
        playerAnimation.Jump(false);
    }

    //Climb is called when the player presses up or down
    void Climb()
    {
        /* climbing actions go here in Part C            
         * float v = Input.GetAxisRaw("Vertical");   determines up or down movement
         * playerAnimation.Climb(v);
        */
    }

    //Attack is called when the player presses 'j'
    void Attack()
    {
        /* attack actions go here in Part C
         * playerAnimation.Attack(true);
         */
    }

    //Flip is called when the player presses left/right keys
    void Flip()
    {
        // Switch the way the player is labelled as facing
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
