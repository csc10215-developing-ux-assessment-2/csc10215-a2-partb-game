﻿/* Title: PlayerAnimation script
 * Author: Jessica McKendry
 * Purpose: Provides functions for animating the player character
 * Last modified: 30/1/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Walk(float walk)
    {
        //sets the animation parameter 'walk'
        animator.SetFloat("walk",Mathf.Abs(walk));
    }

    public void Jump(bool isJumping)
    {
        //sets the animation parameter 'jump'
        animator.SetBool("isJumping", isJumping);
    }

    public void Climb(float climb)
    {
        //sets the animation parameter 'climb'
        animator.SetFloat("climb", Mathf.Abs(climb));
    }

    public void Attack(float attack)
    {
        //sets the animation parameter 'attack'
        animator.SetFloat("attack", Mathf.Abs(attack));
    }
}
