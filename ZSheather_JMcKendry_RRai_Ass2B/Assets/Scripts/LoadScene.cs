﻿/* Title: LoadScene script
 * Author: Zac Sheather
 * Purpose: To provide a function for navigating between scenes (i.e., different screens)
 * Last modified: 28/1/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadScene : MonoBehaviour
{

    public void SceneLoader(int SceneIndex)
    {
        //Debug.Log("sceneBuildIndex to load: " + SceneIndex);
        SceneManager.LoadScene(SceneIndex);
    }
}
