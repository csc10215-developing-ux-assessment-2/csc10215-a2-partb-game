﻿/* Title: CameraController script
 * Author: Jessica McKendry
 * Purpose: Sets the camera to follow the player
 * Last Modified: 01/02/2020
 */
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;   //for getting the player's position

    public float smoothSpeed = 0.125f;  //movement smoothing
    private Vector3 offset;     //player to camera offset
    
    // Start is called before the first frame update
    void Start()
    {
        //find initial position differences between player and camera
        offset = transform.position - player.transform.position;        
    }

    // LateUpdate is called after all Update functions and should be used for follow cameras
    void LateUpdate()
    {
        Vector3 desiredPosition = player.transform.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
    
        //move camera to player's position, accounting for offset
        transform.position = smoothedPosition;
    }

    /*
        private float boundaryX = 100f;
        private float boundaryY = 100f;

        private float minX;
        private float maxX;
        private float minY;
        private float maxY;
              //find game screen bounds
            float vertExtent = Camera.main.camera.orthographicSize;
            float horzExtent = vertExtent * Screen.width / Screen.height;

            // Calculations assume map is position at the origin
            minX = horzExtent - mapX / 2.0;
            maxX = mapX / 2.0 - horzExtent;
            minY = vertExtent - mapY / 2.0;
            maxY = mapY / 2.0 - vertExtent;
         function LateUpdate() {
             var v3 = transform.position;
             v3.x = Mathf.Clamp(v3.x, minX, maxX);
             v3.y = Mathf.Clamp(v3.y, minY, maxY);
             transform.position = v3;
     }
     */
}
